# DemocracyHelper

Backend

## Api

Be aware that the body types are dependent on the call. Check `Web/WebTypes/` for details.

/api (without anything more) indicates that the server is running.


### api/proposals

**GetAll**: Gives all proposals from a given project
    
    [HttpGet] ([FromQuery] Guid projectId)

**Create**: New Proposal for the project defined in the object

    [HttpPost] ([FromBody] PostProposalBody body)

**Update**: Changes attributes of a given proposal

    [HttpPut] ([FromBody] UpdateProposalBody body)

**Delete**: Removes Proposal and connected Votes

    [HttpDelete] ([FromBody] DeleteProposalBody body)


### api/votes

**Get**: Gives back all votes for a given project. If a proposal AND a project is
specified, all votes for that proposal are given back.

    [HttpGet] ([FromQuery] Guid? projectId, [FromQuery] Guid? proposalId)

**Create**: New Vote for the proposal specified in the body. Along with the
proposal, we implicitly specify the project.

    [HttpPost] Create([FromBody] PostVoteBody body)

**Update**: Changes attributes of a given vote.

    [HttpPut] Update([FromBody] UpdateVoteBody body)

**Delete**: Removes Vote
    
    [HttpDelete] ([FromBody] DeleteVoteBody body)


## Configuration

The projects handeled by a server instance are configured in the file `Web/appsettings.json`. Take the given
one as an example and modify it according to your needs. Make sure that the frontend you set up use
a matching configuration.

Mainly, the field "Projects" is important.


## Installation / Usage

### Local development

    dotnet run
    
### Docker

#### Volume

First, we need to create a volume

    docker volume create dh_sqlite_volume
    
Then, we need to put a correctly configured db inside the volume, here using dotnet entity framework:

    docker build -f EF.Dockerfile -t dh_ef_handler_img .
    docker run --rm -it -v dh_sqlite_volume:/app/database -e DH_DB_STRING="/app/database/sqlite.db" dh_ef_handler_img

    # inside the container, run
    dotnet dotnet-ef migrations add InitialCreate
    dotnet dotnet-ef database update
    exit

The volume now contains a database (called sqlite.db). We can mount it later.

Alternatively (on Linux) we can run the script:

    ./initialize_database_in_volume.sh


#### Build and run

Configure appsettings.json similar to example.appsettings.json.

    docker build -t dh_backend_img .
    docker run --rm -p 8080:8080 -v dh_sqlite_volume:/app/database -e DH_DB_STRING="/app/database/sqlite.db" --name dh_backend dh_backend_img

Alternative:
    
    docker compose up

Using an internal network (set up a .env file similar to example.env!)

    docker compose -f docker-compose.internal_network.yml up


## Database

The environment variable DH_DB_STRING defines the path of the SQLite database. At this path,
a database must be present when the app is started.

    dotnet ef migrations add InitialCreate
    dotnet ef database update


## Testing

In integration tests, we have the extension method OverrideProjectsFromAppsettings(List<Project> projects)
(namespace Web) that we can use as follows:

	var testProjects = ...
    _factory = new WebApplicationFactory<Program>()
            // override projects
            .WithWebHostBuilder(builder => builder.OverrideProjectsFromAppsettings(testProjects));

Also, the method TestHelper.TestEnvironment_is_true_or_throw() helps to protect production databases.


## Todos

 - Test Logs. Think about how they should actually be done.
 - Set up SSL for staging server.