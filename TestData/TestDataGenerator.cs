﻿using Entities;

namespace TestData;

public class TestDataGenerator
{
    private readonly List<Project> _projects;
    private readonly List<Vote> _votes;
    private readonly List<Proposal> _proposals;

    public List<Project> GetProjects() => _projects;
    public List<Vote> GetVotes() => _votes;
    public List<Proposal> GetProposals() => _proposals;
    

    public Proposal GetRandomProposal()
        => _proposals[ GetRandomSmallerThan(_proposals!.Count) ];
    
    public Vote GetRandomVote()
        => _votes[ GetRandomSmallerThan(_votes!.Count) ];
        
    public TestDataGenerator()
    {
        #region _projects

        _projects = new()
        {
            new Project()
            {
                Id = Guid.Parse("6f5fc18a-0020-4398-b38b-c2ce22f7d859"),
                ProjectPasswords = ["abc", "def"],
                Name = "Project_1"
            },
            new Project()
            {
                Id = Guid.Parse("09f5cff4-1adb-4f7f-9c0f-ccb3bf9ae711"),
                ProjectPasswords = [ "ghi", "jkl" ],
                Name = "Project_2"
            }
        };

        #endregion

        #region _proposals

        _proposals =
        [
            new Proposal()
            {
                Id = Guid.Parse("aeccbabc-a5cd-4698-92ba-566860aee761"),
                Text = "proposal 1 text",
                ProjectId = Guid.Parse("6f5fc18a-0020-4398-b38b-c2ce22f7d859"),
                CreationPassword = "123",
                Created = new DateTime(),
                LastModified = new DateTime()
            },
            new Proposal()
            {
                Id = Guid.Parse("ba1fd45f-d1fb-4687-9807-a47e6c458e0d"),
                Text = "proposal 1 text",
                ProjectId = Guid.Parse("6f5fc18a-0020-4398-b38b-c2ce22f7d859"),
                CreationPassword = "123",
                Created = new DateTime(),
                LastModified = new DateTime()
            },
            new Proposal()
            {
                Id = Guid.Parse("742bd707-e1f2-437a-8f91-c6e125806b98"),
                Text = "proposal 1 text",
                ProjectId = Guid.Parse("6f5fc18a-0020-4398-b38b-c2ce22f7d859"),
                CreationPassword = "123",
                Created = new DateTime(),
                LastModified = new DateTime()
            },
            new Proposal()
            {
                Id = Guid.Parse("73a9822a-c3ec-49ac-89a5-ca52bf312576"),
                Text = "proposal 1 text",
                ProjectId = Guid.Parse("09f5cff4-1adb-4f7f-9c0f-ccb3bf9ae711"),
                CreationPassword = "123",
                Created = new DateTime(),
                LastModified = new DateTime()
            },
            new Proposal()
            {
                Id = Guid.Parse("5b277d5c-c70f-4d53-8597-17affb0cb99e"),
                Text = "proposal 1 text",
                ProjectId = Guid.Parse("09f5cff4-1adb-4f7f-9c0f-ccb3bf9ae711"),
                CreationPassword = "123",
                Created = new DateTime(),
                LastModified = new DateTime()
            },
        ];

        #endregion

        #region _votes

        _votes =
        [
            new Vote()
            {
                Id = Guid.Parse("94ac638f-6eff-4ba2-8e2e-e982415a72be"),
                Type = new VoteType() { Description = "A" },
                Comment = "vote_1",
                ProposalId = Guid.Parse("aeccbabc-a5cd-4698-92ba-566860aee761"),
                CreationPassword = "123",
                Created = new DateTime(),
                LastModified = new DateTime()
            },
            new Vote()
            {
                Id = Guid.Parse("7b9e5315-cfc9-4377-a1f1-f83c3a83f0a2"),
                Type = new VoteType() { Description = "A" },
                Comment = "vote_2",
                ProposalId = Guid.Parse("aeccbabc-a5cd-4698-92ba-566860aee761"),
                CreationPassword = "123",
                Created = new DateTime(),
                LastModified = new DateTime()
            },
            new Vote()
            {
                Id = Guid.Parse("94ac638f-6eff-4ba2-8e2e-e982415a72be"),
                Type = new VoteType() { Description = "A" },
                Comment = "vote_3",
                ProposalId = Guid.Parse("aeccbabc-a5cd-4698-92ba-566860aee761"),
                CreationPassword = "123",
                Created = new DateTime(),
                LastModified = new DateTime()
            },
            new Vote()
            {
                Id = Guid.Parse("0c33205f-e913-419e-8612-e00f509fa931"),
                Type = new VoteType() { Description = "A" },
                Comment = "vote_4",
                ProposalId = Guid.Parse("ba1fd45f-d1fb-4687-9807-a47e6c458e0d"),
                CreationPassword = "123",
                Created = new DateTime(),
                LastModified = new DateTime()
            },
            new Vote()
            {
                Id = Guid.Parse("09add4cb-3966-4358-be91-345ff164b96d"),
                Type = new VoteType() { Description = "A" },
                Comment = "vote_5",
                ProposalId = Guid.Parse("ba1fd45f-d1fb-4687-9807-a47e6c458e0d"),
                CreationPassword = "123",
                Created = new DateTime(),
                LastModified = new DateTime()
            },
            new Vote()
            {
                Id = Guid.Parse("125a2047-7bd4-4282-b5a4-21be2bfaf82c"),
                Type = new VoteType() { Description = "A" },
                Comment = "vote_5",
                ProposalId = Guid.Parse("ba1fd45f-d1fb-4687-9807-a47e6c458e0d"),
                CreationPassword = "123",
                Created = new DateTime(),
                LastModified = new DateTime()
            },
            new Vote()
            {
                Id = Guid.Parse("d7ea8f25-0db7-4531-af69-37d5f760c879"),
                Type = new VoteType() { Description = "A" },
                Comment = "vote_7",
                ProposalId = Guid.Parse("742bd707-e1f2-437a-8f91-c6e125806b98"),
                CreationPassword = "123",
                Created = new DateTime(),
                LastModified = new DateTime()
            },
            new Vote()
            {
                Id = Guid.Parse("b2b67f58-5776-43e7-a2f5-20fe65240087"),
                Type = new VoteType() { Description = "A" },
                Comment = "vote_8",
                ProposalId = Guid.Parse("742bd707-e1f2-437a-8f91-c6e125806b98"),
                CreationPassword = "123",
                Created = new DateTime(),
                LastModified = new DateTime()
            },
            new Vote()
            {
                Id = Guid.Parse("e7f02756-41c1-4d21-86e1-97cf0bcf89cf"),
                Type = new VoteType() { Description = "A" },
                Comment = "vote_9",
                ProposalId = Guid.Parse("73a9822a-c3ec-49ac-89a5-ca52bf312576"),
                CreationPassword = "123",
                Created = new DateTime(),
                LastModified = new DateTime()
            },
            new Vote()
            {
                Id = Guid.Parse("4672bb41-97ef-415f-a072-bb69328e6306"),
                Type = new VoteType() { Description = "A" },
                Comment = "vote_10",
                ProposalId = Guid.Parse("73a9822a-c3ec-49ac-89a5-ca52bf312576"),
                CreationPassword = "123",
                Created = new DateTime(),
                LastModified = new DateTime()
            },
        ];

        #endregion
    }
    
    private int GetRandomSmallerThan(int maxPlusOne)
        => (int)( new Random().NextDouble() * maxPlusOne );
}