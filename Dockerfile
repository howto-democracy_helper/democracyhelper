# https://hub.docker.com/_/microsoft-dotnet
FROM mcr.microsoft.com/dotnet/sdk:8.0 AS build
WORKDIR /

# copy subprojects one by one
COPY *.sln .
COPY Entities ./Entities
COPY DataBase ./DataBase
COPY Logic ./Logic
COPY Web ./Web


# build app
WORKDIR /Web
RUN dotnet restore
RUN dotnet publish -c release -o /app --no-restore

# final stage/image
FROM mcr.microsoft.com/dotnet/aspnet:8.0
WORKDIR /app
COPY --from=build /app ./
ENTRYPOINT ["dotnet", "Web.dll"]
