namespace Entities;
using Password = string;

public class ClientsideProposal
{
    public string Text { get; set; }
    public Guid ProjectId { get; set; }
    public Guid Id { get; set; }
    public DateTime Created { get; set; }
    public DateTime LastModified { get; set; }

    public override bool Equals(object obj)
    {
        if (obj is not ClientsideProposal) return false;
        var p = (ClientsideProposal)obj;

        return Text == p.Text &&
               ProjectId == p.ProjectId &&
               Id == p.Id &&
               Created == p.Created &&
               LastModified == p.LastModified;
    }
}