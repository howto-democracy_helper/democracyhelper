using System.ComponentModel.DataAnnotations;
using System.Reflection;
using Microsoft.EntityFrameworkCore;

namespace Entities;

public class VoteType
{
    [Required]
    public string Description { get; set; }

    // public enum Type
    // {
    //     HugeFan, //0
    //     SoundsGood, //1
    //     IDontCare, //2
    //     AgainstButNoVeto, //3
    //     Veto, //4
    //     NeedsDiscussion, //5
    // }

    public VoteType()
    {
        Description = string.Empty;
    }

    public VoteType(string description)
    {
        Description = description;
    }
    public override bool Equals(object obj)
    {
        if (obj is not VoteType) return false;
        return ((VoteType)obj).Description == Description;
    }
}