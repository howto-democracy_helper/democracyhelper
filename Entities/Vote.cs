using System.ComponentModel.DataAnnotations;
using Microsoft.EntityFrameworkCore;

namespace Entities;

public class Vote : Statement
{
    [Required]
    public Guid ProposalId { get; set; }
    
    [Required]
    public VoteType Type { get; set; }
    
    [Required]
    public string Comment { get; set; }
}