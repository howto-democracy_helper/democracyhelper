namespace Entities;

public class ClientsideVote
{
    
    public string TypeName { get; set; }
    public string Comment { get; set; }
    public Guid ProjectId { get; set; }
    public Guid ProposalId { get; set; }
    public Guid Id { get; set; }
    public DateTime Created { get; set; }
    public DateTime LastModified { get; set; }
    
    public override bool Equals(object obj)
    {
        if (obj is not ClientsideVote) return false;
        var p = (ClientsideVote)obj;

        return TypeName == p.TypeName &&
               Comment == p.Comment &&
               ProjectId == p.ProjectId &&
               ProposalId == p.ProposalId &&
               Id == p.Id &&
               Created == p.Created &&
               LastModified == p.LastModified;
    }
}