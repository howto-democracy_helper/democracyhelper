﻿using System.ComponentModel.DataAnnotations;

namespace Entities;


public class Proposal : Statement
{
    [Required] public string Text { get; set; }

    [Required] public Guid ProjectId { get; set; }
}