using System.ComponentModel.DataAnnotations;

namespace Entities;
using Password = string;

public class Project
{
    [Required]
    public Guid Id { get; set; }
    
    [Required]
    public string Name { get; set; }
    
    [Required]
    public List<Password> ProjectPasswords { get; set; }
}