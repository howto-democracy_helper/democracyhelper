using System.ComponentModel.DataAnnotations;

namespace Entities;
using Password = string;

public abstract class Statement
{
    [Required]
    public Guid Id { get; set; }
    
    [Required]
    public Password CreationPassword { get; set; }
    
    [Required]
    public DateTime Created { get; set; }
    
    [Required]
    public DateTime LastModified { get; set; }
}