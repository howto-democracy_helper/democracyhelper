namespace DataAccess;

public static class TestHelper
{
    public static void ClearDatabase()
    {
        // precondition
        TestEnvironment_is_true_or_throw();
        
        var db = new AppDataContext();
        db.Projects.RemoveRange(db.Projects);
        db.Proposals.RemoveRange(db.Proposals);
        db.Votes.RemoveRange(db.Votes);
        db.SaveChanges();
    }

    public static void ClearVotesAndProposals()
    {
        // precondition
        TestEnvironment_is_true_or_throw();
        
        var db = new AppDataContext();
        db.Proposals.RemoveRange(db.Proposals);
        db.Votes.RemoveRange(db.Votes);
        db.SaveChanges();
    }
    
    public static void TestEnvironment_is_true_or_throw()
    {
        var testEnv = Environment.GetEnvironmentVariable("TestEnvironment")
            ?? throw new Exception("TestEnvironment is not set");

        if (testEnv != "true") throw new Exception($"TestEnvironment={testEnv}");
    }
}