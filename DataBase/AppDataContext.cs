﻿using Microsoft.EntityFrameworkCore;
using Entities;

namespace DataAccess;

public class AppDataContext : DbContext 
{
    public DbSet<Project> Projects { get; set; }
    public DbSet<Proposal> Proposals { get; set; }
    public DbSet<Vote> Votes { get; set; }

    public string DbPath { get; }
    // public string DbConnectionString { get; } for other databases

    public AppDataContext()
    {
        var dbStringName = "DH_DB_STRING";
        string? dbString = Environment.GetEnvironmentVariable(dbStringName);
        if ( dbString == null )
        {
            throw new Exception($"{dbStringName} not set in environment");
        }
        
        DbPath = dbString;
    }

    // The following configures EF to create a Sqlite database file in the
    // special "local" folder for your platform.
    protected override void OnConfiguring(DbContextOptionsBuilder options)
        => options.UseSqlite($"Data Source={DbPath}");

    protected override void OnModelCreating(ModelBuilder modelBuilder)
    {
        modelBuilder.Entity<Vote>().OwnsOne(p => p.Type);
    }
}