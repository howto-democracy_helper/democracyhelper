
using Microsoft.AspNetCore.HttpLogging;
using Serilog;
using Web;

var builder = WebApplication.CreateBuilder(args);

builder.Services.AddEndpointsApiExplorer();
builder.Services.AddSwaggerGen();
builder.Services.AddControllers();
builder.Services.AddHttpLogging(logging =>
{
    logging.LoggingFields = HttpLoggingFields.All;
});
builder.Logging.AddConsole();

// configure the project entities that will be available in the database
builder.GetProjectsFromAppSettingsAndRegisterLogic();

Log.Logger = new LoggerConfiguration()
    .WriteTo.File("logs/log.txt",
        rollingInterval: RollingInterval.Day,
        rollOnFileSizeLimit: true)
    .CreateLogger();


var app = builder.Build();

// Configure the HTTP request pipeline.
if (app.Environment.IsDevelopment())
{
    app.UseSwagger();
    app.UseSwaggerUI();
}

app.UseHttpsRedirection();
app.MapControllers();
app.UseHttpLogging();

Log.Information("App started");
app.Run();
Log.Information("App stopped");

// expose this class for testing
public partial class Program { }