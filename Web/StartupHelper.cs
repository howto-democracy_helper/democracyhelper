using System.Reflection;
using System.Text.Json;
using Entities;
using Logic;

namespace Web;

public static class StartupHelper
{

    public static void GetProjectsFromAppSettingsAndRegisterLogic(this WebApplicationBuilder builder)
    {
        List<Project> projects = [];
        builder.Configuration.GetSection("Projects").Bind(projects);

        var logic = new AppLogic(projects);
        builder.Services.AddSingleton<ILogic>(logic);
    }

    /// <summary>
    /// Deletes any registered ILogic from dependency services and registers a new instance containing the
    /// projects given as argument. 
    /// </summary>
    /// <param name="builder"></param>
    /// <param name="projects"></param>
    public static void OverrideProjectsFromAppsettings(this IWebHostBuilder builder, List<Project> projects)
    {
        var newLogic = new AppLogic(projects);
        builder.ConfigureServices((context, services) =>
        {
            var descriptor = services.FirstOrDefault(descriptor => descriptor.ServiceType == typeof(ILogic));
            if (descriptor != null) services.Remove(descriptor);

            services.AddSingleton<ILogic>(newLogic);
        });
    }
}