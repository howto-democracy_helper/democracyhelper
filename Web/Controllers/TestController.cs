using Microsoft.AspNetCore.Mvc;

namespace Web.Controllers;

[Route("api/")]
public class TestController : ControllerBase
{
    [HttpGet]
    public IActionResult IsRunning()
    {
        return Ok("server is running!");
    }
}