using Entities;
using Logic;
using Microsoft.AspNetCore.Mvc;
using Web.WebTypes;

namespace Web.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ProposalsController(ILogic logic) : CustomControllerBase(logic)
    {
        [HttpGet]
        public IActionResult GetAll([FromQuery] Guid projectId)
        {
            return BadRequestIfUserCausedException(() =>
            {
                var proposals =
                    Logic.GetProposals(projectId);
                return Ok(proposals);
            });
        }
        
        [HttpPost]
        public IActionResult Create([FromBody] PostProposalBody body)
        {
            return BadRequestIfUserCausedException(() =>
            {
                Logic.AddProposal(body.Proposal, body.Password);
                return Ok();
            });
        }
        
        [HttpPut]
        public IActionResult Update([FromBody] UpdateProposalBody body)
        {
            return BadRequestIfUserCausedException(() =>
            {
                Logic.UpdateProposal(body.NewProposal, body.OldId, body.Password);
                return Ok();
            });
        }
        
        [HttpDelete]
        public IActionResult Delete([FromBody] DeleteProposalBody body)
        {
            return BadRequestIfUserCausedException(() =>
            {
                Logic.DeleteProposal(body.ToBeDeletedId, body.Password);
                return Ok();
            });
        }
    }
}
