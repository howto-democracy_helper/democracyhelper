using Logic;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Web.WebTypes;

namespace Web.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class VotesController(ILogic logic) : CustomControllerBase(logic)
    {
        [HttpGet]
        public IActionResult Get([FromQuery] Guid? projectId, [FromQuery] Guid? proposalId)
        {
            if(projectId is null) return BadRequest();
            var projectId_checked = (Guid)projectId;

            if (proposalId is null) return GetAllOfProject(projectId_checked);
            
            // both not null
            var proposalId_checked = (Guid)proposalId;
            return GetAllOfProposal(proposalId_checked, projectId_checked);
        }
        
        private IActionResult GetAllOfProject(Guid projectId)
        {
            return BadRequestIfUserCausedException(() =>
            {
                var proposals =
                    Logic.GetAllVotes(projectId);
                return Ok(proposals);
            });
        }
        
        private IActionResult GetAllOfProposal(Guid proposalId, Guid projectId)
        {
            return BadRequestIfUserCausedException(() =>
            {
                var proposals =
                    Logic.GetVotesForProposal(proposalId, projectId);
                return Ok(proposals);
            });
        }
        
        [HttpPost]
        public IActionResult Create([FromBody] PostVoteBody body)
        {
            return BadRequestIfUserCausedException(() =>
            {
                Logic.AddVote(body.Vote, body.Password);
                return Ok();
            });
        }
        
        [HttpPut]
        public IActionResult Update([FromBody] UpdateVoteBody body)
        {
            return BadRequestIfUserCausedException(() =>
            {
                Logic.UpdateVote(body.NewVote, body.OldId, body.Password);
                return Ok();
            });
        }
        
        [HttpDelete]
        public IActionResult Delete([FromBody] DeleteVoteBody body)
        {
            return BadRequestIfUserCausedException(() =>
            {
                Logic.DeleteVote(body.ToBeDeletedId, body.ProjectId, body.Password);
                return Ok();
            });
        }
    }
}
