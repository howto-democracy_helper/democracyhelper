using Logic;
using Logic.Exceptions;
using Microsoft.AspNetCore.Mvc;

namespace Web.Controllers;

[ActionLogger]
public class CustomControllerBase(ILogic logic) : ControllerBase
{
    protected readonly ILogic Logic = logic;

    protected IActionResult BadRequestIfUserCausedException(Func<IActionResult> toBeExecuted)
    {
        try
        {
            return toBeExecuted();
        }
        catch (UserCausedException e)
        {
            return BadRequest();
        }
    }
}