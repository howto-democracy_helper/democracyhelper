using Entities;

namespace Web.WebTypes;

public class PostProposalBody
{
    public required ClientsideProposal Proposal { get; set; }
    public required string Password { get; set; }
}