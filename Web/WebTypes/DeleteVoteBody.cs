namespace Web.WebTypes;

public class DeleteVoteBody
{
    public required Guid ToBeDeletedId { get; set; }
    public required Guid ProjectId { get; set; }
    public required string Password { get; set; }
}