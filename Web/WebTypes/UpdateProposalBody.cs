using Entities;

namespace Web.WebTypes;

public class UpdateProposalBody
{

    public required ClientsideProposal NewProposal { get; set; }
    
    public required Guid OldId { get; set; }
    
    public required string Password { get; set; }
}