using Entities;

namespace Web.WebTypes;

public class UpdateVoteBody
{
    public required ClientsideVote NewVote { get; set; }
    public required Guid OldId { get; set; }
    public required string Password { get; set; }
}