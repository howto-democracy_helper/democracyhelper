namespace Web.WebTypes;

public class DeleteProposalBody
{
    public required Guid ToBeDeletedId { get; set; }
    
    public required string Password { get; set; }
}