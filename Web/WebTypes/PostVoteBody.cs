using Entities;

namespace Web.WebTypes;

public class PostVoteBody
{
    public required ClientsideVote Vote { get; set; }
    public required string Password { get; set; }
}