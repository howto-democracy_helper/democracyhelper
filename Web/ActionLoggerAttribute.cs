using Microsoft.AspNetCore.Mvc.Filters;
using Serilog;

namespace Web;

public class ActionLoggerAttribute : ActionFilterAttribute
{
    public override void OnActionExecuting(ActionExecutingContext actionContext)
    {
        var controllerName =
            ((Microsoft.AspNetCore.Mvc.Controllers.ControllerActionDescriptor)actionContext.ActionDescriptor)
            .ControllerName;
        var actionName =
            ((Microsoft.AspNetCore.Mvc.Controllers.ControllerActionDescriptor)actionContext.ActionDescriptor)
            .ActionName;
        Log.Information("Api called: {ControllerName}, {MethodName}!",
            controllerName,
            actionName);
    }

}