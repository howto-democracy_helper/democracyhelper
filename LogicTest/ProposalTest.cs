
using DataAccess;
using Entities;
using TestData;
using Logic;
using Logic.Exceptions;

namespace LogicTest;

[Collection("SequentialExecution")]
public class ProposalTest
{
    private readonly TestDataGenerator _generator;
    private readonly ILogic _testedLogic;

    public ProposalTest()
    {
        TestHelper.TestEnvironment_is_true_or_throw();
        TestHelper.ClearDatabase();
        
        _generator = new TestDataGenerator();
        _testedLogic = new AppLogic(_generator.GetProjects());
    }
    
    #region GetProposal
    [Fact]
    public void GetProposal_after_AddProposal_returns_a_Collection_containing_the_item()
    {
        TestHelper.ClearVotesAndProposals();
        
        var completeItem = _generator.GetRandomProposal();
        var item = AppLogic.ToClientsideProposal(completeItem);
        var projectId = item.ProjectId;
        
        _testedLogic.AddProposal(item, "ignored_password");

        var receivedProposals = _testedLogic.GetProposals(projectId);
        Assert.NotNull(receivedProposals);

        var receivedItem = receivedProposals.First(p => p.Id == item.Id);
        
        Assert.Equal(item.Text, receivedItem.Text);
        Assert.Equal(item.ProjectId, receivedItem.ProjectId);
        Assert.Equal(item.Id, receivedItem.Id);

        // note that Created and LastModified are updated during Logic.AddProposal()
    }
    #endregion

    #region AddProposal and GetProposal
    [Fact]
    public void GetProposals_after_AddProposal_and_DeleteProposal_does_not_contain_the_item()
    {
        TestHelper.ClearVotesAndProposals();
        
        var completeItem = _generator.GetRandomProposal();
        var item = Logic.AppLogic.ToClientsideProposal(completeItem);
        var itemId = item.Id;
        var projectId = item.ProjectId;
        var password = completeItem.CreationPassword;
        
        _testedLogic.AddProposal(item, password);
        _testedLogic.DeleteProposal(itemId, password);

        var receivedProposals = _testedLogic.GetProposals(projectId);
        Assert.NotNull(receivedProposals);

        var matchingProposals = receivedProposals.Select(p => p.Id == itemId);
        Assert.Empty(matchingProposals);
    }
    #endregion
    
    #region Add, Delete, GetProposal(s)
    [Fact]
    public void GetProposals_after_adding_and_deleging_using_ProjectPassword_does_not_contain_the_item()
    {
        TestHelper.ClearVotesAndProposals();
        
        var completeItem = _generator.GetRandomProposal();
        var item = Logic.AppLogic.ToClientsideProposal(completeItem);
        var itemId = item.Id;
        var projectId = item.ProjectId;

        var project = _generator.GetProjects().Find(p => p.Id == item.ProjectId);
        var password = project!.ProjectPasswords[0];
        
        _testedLogic.AddProposal(item, password);
        _testedLogic.DeleteProposal(itemId, password);

        var receivedProposals = _testedLogic.GetProposals(projectId);
        Assert.NotNull(receivedProposals);

        var matchingProposals = receivedProposals.Select(p => p.Id == itemId);
        Assert.Empty(matchingProposals);
    }
    
    [Fact]
    public void Deleting_with_bad_id_trows()
    {
        TestHelper.ClearVotesAndProposals();
        
        var completeItem = _generator.GetRandomProposal();
        var item = Logic.AppLogic.ToClientsideProposal(completeItem);
        var password = completeItem.CreationPassword;
        var badId = Guid.NewGuid();
        
        _testedLogic.AddProposal(item, password);

        Assert.Throws<BadDataException>(() => _testedLogic.DeleteProposal(badId, password));
    }
    
    [Fact]
    public void Deleting_with_bad_password_trows()
    {
        TestHelper.ClearVotesAndProposals();
        
        var completeItem = _generator.GetRandomProposal();
        var item = Logic.AppLogic.ToClientsideProposal(completeItem);
        var itemId = item.Id;
        var passwordFromTestdata = completeItem.CreationPassword;
        var badPassword = passwordFromTestdata + "mistake";
        
        _testedLogic.AddProposal(item, passwordFromTestdata);

        Assert.Throws<BadDataException>(() => _testedLogic.DeleteProposal(itemId, badPassword));
    }
    #endregion

    [Fact]
    public void Add_and_Update_and_Get_returns_the_modified_Item()
    {
        TestHelper.ClearVotesAndProposals();
        
        var completeItem = _generator.GetRandomProposal();
        var initialItem = Logic.AppLogic.ToClientsideProposal(completeItem);
        
        var projectId = initialItem.ProjectId;
        var password = completeItem.CreationPassword;
        
        var newText = initialItem.Text + "modification";
        var newProposal = new ClientsideProposal
        {
            Text = newText,
            ProjectId = initialItem.ProjectId,
            Id = initialItem.Id,
            Created = initialItem.Created,
            LastModified = initialItem.LastModified
        };
        
        _testedLogic.AddProposal(initialItem, password);
        _testedLogic.UpdateProposal(newProposal, initialItem.Id, password);

        var receivedProposals = _testedLogic.GetProposals(projectId);
        Assert.NotNull(receivedProposals);

        var received = receivedProposals.First();
        Assert.Equal(initialItem.Id, received.Id);
        Assert.Equal(initialItem.ProjectId, received.ProjectId);
        Assert.Equal(newText, received.Text);
        // Assert.Equal(initialItem.Created, received.Created); // updated during AddProposal
        Assert.NotEqual(initialItem.LastModified, received.LastModified);
    }
}
