using DataAccess;
using Entities;
using TestData;
using Logic;
using Logic.Exceptions;

namespace LogicTest;

[Collection("SequentialExecution")]
public class VoteTest
{
    private readonly TestDataGenerator _generator;
    private readonly ILogic _testedLogic;
    
    public VoteTest()
    {
        TestHelper.TestEnvironment_is_true_or_throw();
        TestHelper.ClearDatabase();
        
        _generator = new TestDataGenerator();
        _testedLogic = new AppLogic(_generator.GetProjects());
    }
    
    #region Get
    [Fact]
    public void GetAllVotes_initially_returns_an_empty_list()
    {
        var validProjectId = _generator.GetProjects().First().Id;
        
        var received = _testedLogic.GetAllVotes(validProjectId);
        Assert.NotNull(received);
        Assert.Empty(received);
    }
    
    [Fact]
    public void GetAllVotes_with_invalid_ProjectId_throws()
    {
        var invalid = Guid.NewGuid();
        
        Assert.Throws<BadDataException>(() => _testedLogic.GetAllVotes(invalid));
    }
    
    [Fact]
    public void GetAllVotesForProposal_initially_returns_an_empty_list()
    {
        var validProjectId = _generator.GetProjects().First().Id;
        var proposal = AppLogic.ToClientsideProposal(_generator.GetRandomProposal());
        _testedLogic.AddProposal(proposal, "ignored");

        var received = _testedLogic.GetVotesForProposal(proposal.Id, validProjectId);
        
        Assert.NotNull(received);
        Assert.Empty(received);
    }
    
    [Fact]
    public void GetAllVotesForProposal_with_invalid_ProjectId_throws()
    {
        var proposal = AppLogic.ToClientsideProposal(_generator.GetRandomProposal());
        _testedLogic.AddProposal(proposal, "ignored");

        var invalidProjectId = Guid.NewGuid();

        Assert.Throws<BadDataException>(() => _testedLogic.GetVotesForProposal(proposal.Id, invalidProjectId));
    }
    #endregion

    #region Add and Get
    
    [Fact]
    public void GetAllVotes_after_AddVote_gives_back_item()
    {
        TestHelper.ClearVotesAndProposals();

        var voteComplete = _generator.GetRandomVote();
        var proposalComplete = _generator.GetProposals()
            .First(p => p.Id == voteComplete.ProposalId);
        
        var proposal = AppLogic.ToClientsideProposal(proposalComplete);
        var projectId = proposal.ProjectId;
        _testedLogic.AddProposal(proposal, "ignored");

        var vote = AppLogic.ToClientsideVote(voteComplete, projectId);
        _testedLogic.AddVote(vote, "also_ignored");

        var receivedVotes = _testedLogic.GetAllVotes(projectId);
        Assert.NotNull(receivedVotes);

        var received = receivedVotes.First(v => v.Id == vote.Id);
        Assert.NotNull(received);
        Assert.Equal(vote.TypeName, received.TypeName);
        Assert.Equal(vote.Comment, received.Comment);
        Assert.Equal(vote.ProjectId, received.ProjectId);
        Assert.Equal(vote.ProposalId, received.ProposalId);
    }
    
    [Fact]
    public void GetVotesForProposal_after_AddVote_gives_back_item_only_for_correct_project()
    {
        TestHelper.ClearVotesAndProposals();
        
        var voteComplete = _generator.GetRandomVote();
        var proposalComplete = _generator.GetProposals()
            .First(p => p.Id == voteComplete.ProposalId);
        
        var proposal = AppLogic.ToClientsideProposal(proposalComplete);
        var projectId = proposal.ProjectId;
        _testedLogic.AddProposal(proposal, "ignored");

        var otherProject = _generator.GetProjects().First(p => p.Id != projectId);
        var otherProjectId = otherProject.Id;
        
        var vote = AppLogic.ToClientsideVote(voteComplete, projectId);
        _testedLogic.AddVote(vote, "also_ignored");

        var receivedVotes = _testedLogic.GetVotesForProposal(proposal.Id, projectId);
        Assert.NotNull(receivedVotes);

        var received = receivedVotes.First(v => v.Id == vote.Id);
        Assert.NotNull(received);
        Assert.Equal(vote.TypeName, received.TypeName);
        Assert.Equal(vote.Comment, received.Comment);
        Assert.Equal(vote.ProjectId, received.ProjectId);
        Assert.Equal(vote.ProposalId, received.ProposalId);
        
        // wrong project id
        Assert.Throws<BadDataException>(() => _testedLogic.GetVotesForProposal(proposal.Id, otherProjectId));
        
        // invalid guid
        Assert.Throws<BadDataException>(() => _testedLogic.GetVotesForProposal(Guid.NewGuid(), projectId));
    }
    #endregion
    
    #region Update

    [Fact]
    void Update_modifies_the_item_as_expected()
    {
        TestHelper.ClearVotesAndProposals();
        
        var voteComplete = _generator.GetRandomVote();
        var proposalComplete = _generator.GetProposals()
            .First(p => p.Id == voteComplete.ProposalId);
        
        var proposal = AppLogic.ToClientsideProposal(proposalComplete);
        var projectId = proposal.ProjectId;
        _testedLogic.AddProposal(proposal, "ignored");
        
        var initialVote = AppLogic.ToClientsideVote(voteComplete, projectId);
        var initialPassword = voteComplete.CreationPassword;
        _testedLogic.AddVote(initialVote, initialPassword);

        var newModified = new ClientsideVote
        {
            TypeName = "newType",
            Comment = "newComment",
            ProjectId = initialVote.ProjectId,
            ProposalId = initialVote.ProposalId,
            Id = Guid.NewGuid(),                        // will all be ignored
            Created = initialVote.Created,              // ''
            LastModified = initialVote.LastModified     // ''
        };
        
        _testedLogic.UpdateVote(newModified, initialVote.Id, initialPassword);

        var receivedVotes = _testedLogic.GetVotesForProposal(proposal.Id, projectId);
        Assert.NotNull(receivedVotes);

        var received = receivedVotes.First(v => v.Id == initialVote.Id);
        Assert.NotNull(received);
        Assert.Equal(newModified.TypeName, received.TypeName);
        Assert.Equal(newModified.Comment, received.Comment);
        Assert.Equal(initialVote.ProjectId, received.ProjectId);
        Assert.Equal(initialVote.ProposalId, received.ProposalId);
    }
    #endregion
    
    #region Delete

    [Fact]
    public void DeleteVote_removes_earlier_added_Vote_if_arguments_are_correct()
    {
        TestHelper.ClearVotesAndProposals();
        
        var voteComplete = _generator.GetRandomVote();
        var proposalComplete = _generator.GetProposals()
            .First(p => p.Id == voteComplete.ProposalId);
        
        var proposal = AppLogic.ToClientsideProposal(proposalComplete);
        var projectId = proposalComplete.ProjectId;
        _testedLogic.AddProposal(proposal, "ignored");
        
        var initialVote = AppLogic.ToClientsideVote(voteComplete, projectId);
        var initialPassword = voteComplete.CreationPassword;
        _testedLogic.AddVote(initialVote, initialPassword);
        
        // bad vote id
        Assert.Throws<BadDataException>(() =>
            _testedLogic.DeleteVote(Guid.NewGuid(), projectId, initialPassword));
        
        // bad vote id
        Assert.Throws<BadDataException>(() =>
            _testedLogic.DeleteVote(initialVote.Id, Guid.NewGuid(), initialPassword));
        
        // item must still be there
        var receivedVotes = _testedLogic.GetVotesForProposal(proposal.Id, projectId);
        Assert.NotNull(receivedVotes);
        var ignored = receivedVotes
            .First(v => v.Id == initialVote.Id); // implicit does not throw
        
        // correct params
        _testedLogic.DeleteVote(initialVote.Id, projectId, initialPassword);
        receivedVotes = _testedLogic.GetVotesForProposal(proposal.Id, projectId);
        Assert.NotNull(receivedVotes);

        // item must be gone
        Assert.Throws<InvalidOperationException>(() => 
            receivedVotes.First(v => v.Id == initialVote.Id));
    }
    #endregion
}