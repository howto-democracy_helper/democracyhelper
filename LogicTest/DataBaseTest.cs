
using DataAccess;
using Entities;
using TestData;
using Xunit.Sdk;

namespace LogicTest;

[Collection("SequentialExecution")]
public class DataBaseTest
{
    private readonly AppDataContext _context;
    private readonly TestDataGenerator _generator;

    public DataBaseTest()
    {
        TestHelper.TestEnvironment_is_true_or_throw();
        TestHelper.ClearDatabase();
        _context = new AppDataContext();
        
        _generator = new TestDataGenerator();
    }

    [Fact]
    public void Add_Project()
    {
        var item = _generator.GetProjects().First();
        
        _context.Projects.Add(item);
        _context.SaveChanges();
    }
    
    [Fact]
    public void Add_Proposal()
    {
        var item = _generator.GetProposals().First();
        
        _context.Proposals.Add(item);
        _context.SaveChanges();
    }
    
    [Fact]
    public void Add_Vote()
    {
        var item = _generator.GetVotes().First();
        
        _context.Votes.Add(item);
        _context.SaveChanges();
    }

    [Fact]
    public void Add_And_Retrieve_Vote()
    {
        var item = _generator.GetVotes().Skip(1).First();
        
        _context.Votes.Add(item);
        _context.SaveChanges();

        var retrieved = _context.Votes.First(v => v.Id == item.Id);

        Assert.NotNull(retrieved);
        Assert.Equal(item.ProposalId, retrieved.ProposalId);
        Assert.Equal(item.Type, retrieved.Type);
        Assert.Equal(item.Comment, retrieved.Comment);
        Assert.Equal(item.CreationPassword, retrieved.CreationPassword);
        Assert.Equal(item.Created, retrieved.Created);
        Assert.Equal(item.LastModified, retrieved.LastModified);
    }
    
}

