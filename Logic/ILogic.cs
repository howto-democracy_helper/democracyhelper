using Entities;
using Password = string;

namespace Logic;

public interface ILogic
{
    #region Proposals
    public IEnumerable<ClientsideProposal> GetProposals(Guid projectId);
    
    public void AddProposal(ClientsideProposal proposal, Password newPassword);

    public void UpdateProposal(ClientsideProposal newProposal, Guid oldId, Password password);
    
    public void DeleteProposal(Guid id, Password password);
    #endregion
    
    #region Votes
    public IEnumerable<ClientsideVote> GetAllVotes(Guid projectId);
    public IEnumerable<ClientsideVote> GetVotesForProposal(Guid proposalId, Guid projectId);
    
    public void AddVote(ClientsideVote vote, Password newPassword);
    
    public void UpdateVote(ClientsideVote newVote, Guid oldId, Password password);

    public void DeleteVote(Guid voteId, Guid projectId, Password password);

    #endregion


}