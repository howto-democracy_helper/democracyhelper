﻿using DataAccess;
using Entities;
using Logic.Exceptions;
using Microsoft.EntityFrameworkCore;
using Password = string;

namespace Logic;

public class AppLogic : ILogic
{
    private readonly AppDataContext _db = new();

    public AppLogic(IEnumerable<Project> projects)
    {
        SetProjects(projects, true);
    }
    
    #region Projects
    private void SetProjects(IEnumerable<Project> projects, bool deleteExisting)
    {
        if (deleteExisting)
            _db.Projects.RemoveRange(_db.Projects);
        
        _db.Projects.AddRange(projects);
        _db.SaveChanges();
    }
    #endregion
    
    #region Proposals
    public IEnumerable<ClientsideProposal> GetProposals(Guid projectId)
    {
        if (InvalidProjectId(projectId))
            throw new BadDataException("invalid project id");
        
        var list = _db.Proposals.Where(p => p.ProjectId == projectId).ToList();
        return ToClientsideProposalList(list);
    }

    public void AddProposal(ClientsideProposal proposal, Password newPassword)
    {
        if (InvalidProjectId(proposal.ProjectId))
            throw new BadDataException("invalid project id");
        
        if (ProposalAlreadyPresent(proposal.Id))
            throw new BadDataException("proposal already present");
        
        _db.Proposals.Add(new Proposal
        {
            Id = proposal.Id,
            CreationPassword = newPassword,
            Created = DateTime.Now,
            LastModified = DateTime.Now,
            Text = proposal.Text,
            ProjectId = proposal.ProjectId
        });
        _db.SaveChanges();
    }

    public void UpdateProposal(ClientsideProposal newProposal, Guid oldId, Password password)
    {
        // preconditions
        if (InvalidProjectId(newProposal.ProjectId))
            throw new BadDataException("invalid project id");

        var oldProposal = TryGetOldProposal(oldId) ??
            throw new BadDataException("proposal to modify not found");
        
        if (InvalidPassword(oldProposal, password))
            throw new BadDataException("password not valid");
        
        // update
        oldProposal.LastModified = DateTime.Now;
        oldProposal.Text = newProposal.Text;
        
        _db.SaveChanges();
    }

    public void DeleteProposal(Guid id, Password password)
    {
        // preconditions
        var toBeDeleted = TryGetOldProposal(id) ?? 
            throw new BadDataException("proposal to be deleted not found");
        
        if (InvalidPassword(toBeDeleted, password))
            throw new BadDataException("password not valid");
        
        // delete
        _db.Remove(toBeDeleted);
        _db.SaveChanges();
    }
    
    #endregion

    #region Votes
    public IEnumerable<ClientsideVote> GetAllVotes(Guid projectId)
    {
        if(InvalidProjectId(projectId))
            throw new BadDataException("invalid project id");
        
        var proposalList = _db.Proposals.Where(p => p.ProjectId == projectId).ToList();
        var toBeReturned = new List<ClientsideVote>();
        
        proposalList.ForEach( proposal =>
        {
            var votesOfProposal = GetVotesForProposalUnguarded(proposal.Id, projectId);
            toBeReturned.AddRange(votesOfProposal);
        });

        return toBeReturned;
    }

    public IEnumerable<ClientsideVote> GetVotesForProposal(Guid proposalId, Guid projectId)
    {
        if(InvalidProjectId(projectId))
            throw new BadDataException("invalid project id");

        if (InvalidProposalIdForProject(proposalId, projectId))
            throw new BadDataException("proposal id not found in this project");
        
        return GetVotesForProposalUnguarded(proposalId, projectId);
    }

    public void AddVote(ClientsideVote vote, Password newPassword)
    {
        if (InvalidProjectId(vote.ProjectId))
            throw new BadDataException("invalid project id");
        
        if (VoteAlreadyPresent(vote.Id))
            throw new BadDataException("vote already present");
        
        _db.Votes.Add(new Vote
        {
            Id = vote.Id,
            CreationPassword = newPassword,
            Created = DateTime.Now,
            LastModified = DateTime.Now,
            ProposalId = vote.ProposalId,
            Type = new VoteType(vote.TypeName),
            Comment = vote.Comment
        });
        _db.SaveChanges();
    }

    public void UpdateVote(ClientsideVote newVote, Guid oldId, Password password)
    {
        // preconditions
        if (InvalidProposalIdForProject(newVote.ProposalId, newVote.ProjectId))
            throw new BadDataException("invalid project id");

        var oldVote = TryGetOldVote(oldId) ??
            throw new BadDataException("proposal to modify not found");
        
        if (InvalidPassword(oldVote, newVote.ProjectId, password))
            throw new BadDataException("password not valid");
        
        // update
        oldVote.LastModified = DateTime.Now;
        oldVote.Type = new VoteType(newVote.TypeName);
        oldVote.Comment = newVote.Comment;
        
        _db.SaveChanges();
    }

    public void DeleteVote(Guid voteId, Guid projectId, Password password)
    {
        // preconditions
        var toBeDeleted = TryGetOldVote(voteId) ?? 
            throw new BadDataException("proposal to be deleted not found");
        
        if (InvalidProposalIdForProject(toBeDeleted.ProposalId, projectId))
            throw new BadDataException("invalid project id");
        
        if (InvalidPassword(toBeDeleted, projectId, password))
            throw new BadDataException("password not valid");
        
        // delete
        _db.Remove(toBeDeleted);
        _db.SaveChanges();
    }
    #endregion
    
    #region helper methods

    private static List<ClientsideProposal> ToClientsideProposalList(List<Proposal> list)
    {
        List<ClientsideProposal> result = [];

        list.ForEach(p =>
        {
            result.Add(ToClientsideProposal(p));
        });
        
        return result;
    }
    
    // public so it can be used in tests
    public static ClientsideProposal ToClientsideProposal(Proposal p)
    {
        return new ClientsideProposal()
        {
            Id = p.Id,
            ProjectId = p.ProjectId,
            Text = p.Text,
            Created = p.Created,
            LastModified = p.LastModified
        };
    }
    
    private static List<ClientsideVote> ToClientsideVoteList(List<Vote> list, Guid projectId)
    {
        List<ClientsideVote> result = [];

        list.ForEach(v =>
        {
            result.Add(ToClientsideVote(v, projectId));
        });
        
        return result;
    }
    
    // public so it can be used in tests
    public static ClientsideVote ToClientsideVote(Vote v, Guid projectId)
    {
        return new ClientsideVote
        {
            TypeName = v.Type.Description,
            Comment = v.Comment,
            ProjectId = projectId,
            ProposalId = v.ProposalId,
            Id = v.Id,
            Created = v.Created,
            LastModified = v.LastModified
        };
    }
    
    private bool InvalidProjectId(Guid projectId)
    {
        try
        {
            var _ = _db.Projects.First( p => p.Id == projectId);
        }
        catch (Exception){ return true; }

        return false;
    }

    private bool InvalidProposalIdForProject(Guid proposalId, Guid projectId)
    {
        try
        {
            var _ = _db.Proposals.First(p => p.Id == proposalId && p.ProjectId == projectId);
        }
        catch (Exception)
        {
            return true;
        }

        return false;
    }

    private Proposal? TryGetOldProposal(Guid id)
    {
        try
        {
             return _db.Proposals.First(p => p.Id == id);
        }
        catch (Exception)
        {
            return null;
        }
    }
    
    private Vote? TryGetOldVote(Guid id)
    {
        try
        {
            return _db.Votes.First(v => v.Id == id);
        }
        catch (Exception)
        {
            return null;
        }
    }

    private bool InvalidPassword(Proposal proposal, Password password)
    {
        try
        {
            var project = _db.Projects.First( project => project.Id == proposal.ProjectId );
            if(project.ProjectPasswords.Contains(password))
                return false;

            if (proposal.CreationPassword == password)
                return false;
        }
        catch (Exception) { /* ignored */ }

        return true;
    }
    
    private bool InvalidPassword(Vote vote, Guid projectId, Password password)
    {
        try
        {
            var project = _db.Projects.First(p => p.Id == projectId);
            if(project.ProjectPasswords.Contains(password))
                return false;

            if (vote.CreationPassword == password)
                return false;
        }
        catch (Exception) { /* ignored */ }

        return true;
    }

    private bool ProposalAlreadyPresent(Guid id)
    {
        var found = _db.Proposals
            .Where(p => p.Id == id).ToList();
        return found.Count != 0;
    }
    
    private bool VoteAlreadyPresent(Guid id)
    {
        var found = _db.Votes
            .Where(v => v.Id == id).ToList();
        return found.Count != 0;
    }

    private IEnumerable<ClientsideVote> GetVotesForProposalUnguarded(Guid proposalId, Guid projectId)
    {
        var list = _db.Votes.Where(v => v.ProposalId == proposalId).ToList();
        return ToClientsideVoteList(list, projectId);
    }
    
    #endregion
}