namespace Logic.Exceptions;

public class BadDataException(string msg) : UserCausedException(msg);