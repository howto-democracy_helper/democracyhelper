namespace Logic.Exceptions;

public class UserCausedException(string msg) : Exception(msg);