using System.Collections.Immutable;
using System.Net.Http.Json;
using DataAccess;
using Logic;
using Microsoft.AspNetCore.Mvc.Testing;
using TestData;
using Web.WebTypes;
using Web;
using Entities;
using Microsoft.Extensions.DependencyInjection;

namespace WebTest;

[Collection("SequentialExecution")]
public class ApiProposalsTest
{
    private readonly WebApplicationFactory<Program> _factory;
    private readonly string _baseUrl = "api/proposals/";
    private readonly TestDataGenerator _generator = new();

    public ApiProposalsTest()
    {
        List<Project> testProjects = new TestDataGenerator().GetProjects();
        
        _factory = new WebApplicationFactory<Program>()
            // override projects
            .WithWebHostBuilder(builder => builder.OverrideProjectsFromAppsettings(testProjects));
        
        // make sure we don't delete any production data
        TestHelper.TestEnvironment_is_true_or_throw();
    }
    
    [Fact]
    public async Task Get_EndpointsReturnSuccessAndCorrectContentType()
    {
        // Arrange
        var client = _factory.CreateClient();
        var validProjectId = _generator.GetProjects().First().Id;
        TestHelper.ClearVotesAndProposals();

        // Act
        var response = await client.GetAsync(_baseUrl + "?projectId=" + validProjectId.ToString());

        // Assert
        response.EnsureSuccessStatusCode(); // Status Code 200-299
        Assert.Equal("application/json; charset=utf-8", 
            response.Content.Headers.ContentType.ToString());
    }
    
    [Fact]
    public async Task Post_Gets_Ok_with_valid_request()
    {
        // Arrange
        var client = _factory.CreateClient();
        var fullProposal = _generator.GetRandomProposal();
        var content = new PostProposalBody
        {
            Proposal = AppLogic.ToClientsideProposal(fullProposal),
            Password = fullProposal.CreationPassword
        };

        // Act
        var response = await client.PostAsync(_baseUrl, JsonContent.Create(content));

        // Assert
        response.EnsureSuccessStatusCode(); // Status Code 200-299
    }
}