#!/bin/bash

# make sure these parameters match the ones in docker compose!
volume_name="dh_sqlite_volume";
database_string="/app/database/sqlite.db";

sleep_time=8;
# $sleep_time this is necessary so that the container that initializes the db (docker run ...) does not exit before 
# finishing ist task and we don't continue the script too soon.

if ! docker volume ls | grep $volume_name ; then
  echo "We create a new volume:"
  docker volume create $volume_name
  
  echo "We initialize the volume as required..."
  docker build -f EF.Dockerfile -t dh_ef_handler_img .
  
  command_to_execute="dotnet dotnet-ef migrations add InitialCreate && dotnet dotnet-ef database update && sleep $sleep_time"
  
  docker run --rm -d -v $volume_name:/app/database \
    -e DH_DB_STRING=$database_string dh_ef_handler_img /bin/bash -c "$command_to_execute"
    
  # wait a little longer than the container waits
  sleep $(($sleep_time + 1))
  
else
  echo "The volume $volume_name already exists."
  echo "We assume that it had been initialized correctly or even used before."
fi

echo "The database volume should be correctly initialized now."

