# https://hub.docker.com/_/microsoft-dotnet
FROM mcr.microsoft.com/dotnet/sdk:8.0 AS build
WORKDIR /

COPY Entities ./Entities
COPY DataBase ./DataBase

WORKDIR /DataBase
RUN rm -rf Migrations
RUN dotnet new tool-manifest
RUN dotnet tool install dotnet-ef
RUN dotnet restore



